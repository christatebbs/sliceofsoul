//
//  AppDelegate.h
//  SliceOfSoul
//
//  Created by Christiana Tebbs on 3/5/19.
//  Copyright © 2019 Christiana Tebbs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

