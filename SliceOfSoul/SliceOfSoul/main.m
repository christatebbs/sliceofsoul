//
//  main.m
//  SliceOfSoul
//
//  Created by Christiana Tebbs on 3/5/19.
//  Copyright © 2019 Christiana Tebbs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
