//
//  ViewController.h
//  SliceOfSoul
//
//  Created by Christiana Tebbs on 3/5/19.
//  Copyright © 2019 Christiana Tebbs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>

@interface ViewController : UIViewController

@end
