//
//  ViewController.m
//  SliceOfSoul
//
//  Created by Christiana Tebbs on 3/5/19.
//  Copyright © 2019 Christiana Tebbs. All rights reserved.
//

#import "ViewController.h"
#import "IOCMath.h"

@interface ViewController () <ARSCNViewDelegate>

@property (nonatomic, strong) IBOutlet ARSCNView *sceneView;

@property (strong, nonatomic) IOCMath *mathHelper;

@property (nonatomic) CGPoint panStartPos;
@property (nonatomic) CGPoint panCurrentPos;
@property (nonatomic) int swipeCount;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognizer;

@property (nonatomic) float panDeltaX;
@property (nonatomic) float panDeltaY;
@property (nonatomic) float panDeltaZ;

@property (nonatomic) float objectDisplacementX;
@property (nonatomic) float objectDisplacementY;

@property (nonatomic) SCNNode *wokNode;
@property (nonatomic) SCNNode *garlicNode;
@property (nonatomic) SCNNode *greenbeanNode;

@property (nonatomic) float sceneNumber;
@property (nonatomic) float objectX;
@property (weak, nonatomic) IBOutlet UIImageView *stepOne;

@end

    
@implementation ViewController

-(void) setup{
    [self setupScene];
    self.mathHelper = [[IOCMath alloc] init];
}

- (void) setupScene
{
    self.sceneNumber = 0; 
    self.sceneView.delegate = self;
    self.sceneView.showsStatistics = YES;
    self.sceneView.debugOptions = ARSCNDebugOptionShowWorldOrigin;
    self.sceneView.autoenablesDefaultLighting = YES;
    
    SCNScene *scene = [SCNScene sceneNamed:@"art.scnassets/sceneOne.scn"];
    
    self.sceneView.scene = scene;
    
   //- (SCNNode *)childNodeWithName:(NSString *)name recursively:(BOOL)recursively;
    
    self.garlicNode = [scene.rootNode childNodeWithName:@"Garlic" recursively:YES];
    self.wokNode = [scene.rootNode childNodeWithName:@"wok" recursively:YES];
    self.greenbeanNode = [scene.rootNode childNodeWithName:@"greenbeans" recursively:YES];
    
    //self.garlicNode.position = SCNVector3Make(10.0, 10.0, -2.0);

    self.panStartPos = CGPointZero;
    self.panCurrentPos = CGPointZero;
    self.panDeltaX = 0;
    self.panDeltaY = 0;
}

- (void) sceneThree{
    self.stepOne.hidden = YES;
    self.sceneNumber = 3;
    self.sceneView.delegate = self;
    self.sceneView.showsStatistics = YES;
    self.sceneView.debugOptions = ARSCNDebugOptionShowWorldOrigin;
    self.sceneView.autoenablesDefaultLighting = YES;
    
    SCNScene *scene = [SCNScene sceneNamed:@"art.scnassets/sceneOne.scn"];
    
    self.sceneView.scene = scene;
    
    //- (SCNNode *)childNodeWithName:(NSString *)name recursively:(BOOL)recursively;
    
    self.garlicNode = [scene.rootNode childNodeWithName:@"Garlic" recursively:YES];
    self.wokNode = [scene.rootNode childNodeWithName:@"wok" recursively:YES];
    self.greenbeanNode = [scene.rootNode childNodeWithName:@"greenbeans" recursively:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
}

- (void)setupSession
{
    ARWorldTrackingConfiguration *configuration = [ARWorldTrackingConfiguration new];
    [self.sceneView.session runWithConfiguration:configuration];
}


- (IBAction)userDidPan:(UIPanGestureRecognizer *)sender
{
    if(sender.state == UIGestureRecognizerStateBegan){
        self.panStartPos = [sender locationInView:self.view];
        self.panCurrentPos = self.panStartPos;
    } else if (sender.state == UIGestureRecognizerStateEnded){
        NSLog(@"Pan ended");
        self.panCurrentPos = [sender locationInView:self.view];
        if(self.sceneNumber == 2){
            if(self.panCurrentPos.x - self.panStartPos.x > 30 || self.panCurrentPos.x - self.panStartPos.x < -30){
                NSLog(@"You swiped!");
            }
            self.swipeCount++;
            if(self.swipeCount > 4){
                [self sceneThree];
            }
        }
    }

    /*
    NSLog(@"You panned!");
    
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        
        self.panStartPos = [sender locationInView:self.view];
        self.panCurrentPos = self.panStartPos;
        
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        
    }
    
    //Find the current position
    self.panCurrentPos = [sender locationInView:self.view];
    
    //Find the difference between the start position and current position

    self.panDeltaX = self.panCurrentPos.x - self.panStartPos.x;
    self.panDeltaY = self.panCurrentPos.y - self.panStartPos.y;
    
    //self.panDeltaz = fabs(self.panCurrentPos.z - self.panStartPos.z);
    

    
    self.objectDisplacementX = [self.mathHelper getMappedValueWithInput:self.panDeltaX minInput:0 maxInput:100 minOutput:0 maxOutput:2];
    self.objectDisplacementY = [self.mathHelper getMappedValueWithInput:self.panDeltaY minInput:0 maxInput:100 minOutput:0 maxOutput:2];
    
    //Creates a SCNVector3 value for the amount the object should move
    SCNVector3 delta = SCNVector3Make(self.objectDisplacementX/100, -self.objectDisplacementY/100, 0);

    NSLog(@"deltaX: %.2f | deltaY: %.2f", self.objectDisplacementX, self.objectDisplacementY);
    
    //Creates the action to move the object based on the SCNVector3 delta
    SCNAction *moveObjects = [SCNAction moveBy:delta duration:0.0];
    [self.garlicNode runAction:moveObjects];
    
    self.panDeltaY = 0.0;
    self.panDeltaX = 0.0;
    
    //Runs the action on the garlic node
    */

}



- (IBAction)userDidSwipe:(UISwipeGestureRecognizer *)sender {
    NSLog(@"User has swiped!");
    if(self.sceneNumber == 2){
        if(self.swipeCount > 4){
            [self sceneThree];
        } else{
            self.swipeCount++;
            NSLog(@"User has swiped %i times!", self.swipeCount);
        }
    }
}



- (void) sceneTwo{
    self.sceneNumber = 2;
    self.sceneView.delegate = self;
    self.sceneView.showsStatistics = YES;
    self.sceneView.debugOptions = ARSCNDebugOptionShowWorldOrigin;
    self.sceneView.autoenablesDefaultLighting = YES;
    
    SCNScene *scene = [SCNScene sceneNamed:@"art.scnassets/sceneTwo.scn"];
    
    self.sceneView.scene = scene;
    
    //- (SCNNode *)childNodeWithName:(NSString *)name recursively:(BOOL)recursively;
    
    self.garlicNode = [scene.rootNode childNodeWithName:@"Garlic" recursively:YES];
    self.wokNode = [scene.rootNode childNodeWithName:@"wok" recursively:YES];
    self.greenbeanNode = [scene.rootNode childNodeWithName:@"greenbeans" recursively:YES];
}

- (IBAction)userDidTap:(UITapGestureRecognizer *)sender {
    NSLog(@"user tapped!");
    CGPoint tapLocation = [sender locationInView:self.sceneView];
    NSArray *hitTestResults = [self.sceneView hitTest:tapLocation options:nil]; // returns SCNHitTesResult and NOT ARHitTestResult!
    if (hitTestResults.count > 0) {
        SCNHitTestResult *result = hitTestResults[0];
        NSString *nodeName = result.node.name;
        NSLog(@"You tapped on the %@", nodeName);
    }
    if(self.sceneNumber == 0){
        if (hitTestResults.count > 0) {
            SCNHitTestResult *result = hitTestResults[0];
            NSString *nodeName = result.node.name;
            if([nodeName isEqualToString:@"greenbeans"]){
                self.sceneNumber++;
                [self sceneTwo];
            }
           // NSLog(@"You tapped on the %@", nodeName);
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Create a session configuration
    ARWorldTrackingConfiguration *configuration = [ARWorldTrackingConfiguration new];

    // Run the view's session
    [self.sceneView.session runWithConfiguration:configuration];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Pause the view's session
    [self.sceneView.session pause];
}

#pragma mark - ARSCNViewDelegate

/*
// Override to create and configure nodes for anchors added to the view's session.
- (SCNNode *)renderer:(id<SCNSceneRenderer>)renderer nodeForAnchor:(ARAnchor *)anchor {
    SCNNode *node = [SCNNode new];
 
    // Add geometry to the node...
 
    return node;
}
*/

- (void)session:(ARSession *)session didFailWithError:(NSError *)error {
    // Present an error message to the user
    
}

- (void)sessionWasInterrupted:(ARSession *)session {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
    
}

- (void)sessionInterruptionEnded:(ARSession *)session {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
    
}

@end
